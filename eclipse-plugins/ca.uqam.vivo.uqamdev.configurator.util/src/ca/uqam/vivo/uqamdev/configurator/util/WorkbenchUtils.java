package ca.uqam.vivo.uqamdev.configurator.util;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.progress.UIJob;

public class WorkbenchUtils {
	private static int restartCounter=0;
	private static int TOTAL_NUMBER_OF_RESTARTER_CONFIGURATOR = 2;


	/**
	 * Cette méthode compte le nombre d'appel avant de redémarrer Eclipse.
	 * Chaque installer appel cette méthode après avoir réalisé sa tache.
	 * 
	 */
	public static void readyToRestart(){
		restartCounter++;
		if (restartCounter == TOTAL_NUMBER_OF_RESTARTER_CONFIGURATOR ) 
			restart();
	}

	private static void restart() {
		new UIJob("Switching perspectives"){
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				Display.getDefault().asyncExec(new Runnable() {
					public void run() {
						IWorkbench workbench = PlatformUI.getWorkbench();
						IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
						//						try {
						//							workbench.showPerspective("org.topbraidcomposer.ui.TopBraidPerspective", window);
						//							workbench.showPerspective("org.eclipse.debug.ui.DebugPerspective", window);
						//							workbench.showPerspective("org.eclipse.egit.ui.GitRepositoryExploring", window);
						//							workbench.showPerspective("org.eclipse.ui.resourcePerspective", window);
						//							workbench.showPerspective("org.eclipse.jdt.ui.JavaPerspective", window);
						//							workbench.getPerspectiveRegistry().setDefaultPerspective("org.eclipse.jdt.ui.JavaPerspective");
						//						} catch (WorkbenchException e) {
						//							// TODO Auto-generated catch block
						//							e.printStackTrace();
						//						}
						PlatformUI.getWorkbench().restart();
					}
				});
				return Status.OK_STATUS;
			}}
		.run(new NullProgressMonitor());
	}
}
