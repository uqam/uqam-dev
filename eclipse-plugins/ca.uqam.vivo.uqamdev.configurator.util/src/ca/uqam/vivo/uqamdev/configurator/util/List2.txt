java.vendor: Oracle Corporation
osgi.bundles.defaultStartLevel: 4
sun.java.launcher: SUN_STANDARD
org.osgi.supports.framework.extension: true
sun.management.compiler: HotSpot 64-Bit Tiered Compilers
eclipse.p2.profile: epp.package.committers
os.name: Windows 10
sun.boot.class.path: C:\06-Java\jdk1.8.0_171\jre\lib\resources.jar;C:\06-Java\jdk1.8.0_171\jre\lib\rt.jar;C:\06-Java\jdk1.8.0_171\jre\lib\sunrsasign.jar;C:\06-Java\jdk1.8.0_171\jre\lib\jsse.jar;C:\06-Java\jdk1.8.0_171\jre\lib\jce.jar;C:\06-Java\jdk1.8.0_171\jre\lib\charsets.jar;C:\06-Java\jdk1.8.0_171\jre\lib\jfr.jar;C:\06-Java\jdk1.8.0_171\jre\classes
osgi.ws: win32
sun.desktop: windows
java.vm.specification.vendor: Oracle Corporation
java.runtime.version: 1.8.0_171-b11
osgi.instance.area: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/runtime-EclipseApplication/
org.osgi.framework.uuid: 5fd28c61-019b-4e08-ab42-c4bfc0700046
osgi.nl.user: fr_CA
user.name: Michel Heon
osgi.framework.extensions: reference:file:C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.osgi.compatibility.state_1.0.200.v20160504-1419.jar
org.osgi.framework.system.packages: javax.accessibility,javax.activation,javax.activity,javax.annotation,javax.annotation.processing,javax.crypto,javax.crypto.interfaces,javax.crypto.spec,javax.imageio,javax.imageio.event,javax.imageio.metadata,javax.imageio.plugins.bmp,javax.imageio.plugins.jpeg,javax.imageio.spi,javax.imageio.stream,javax.jws,javax.jws.soap,javax.lang.model,javax.lang.model.element,javax.lang.model.type,javax.lang.model.util,javax.management,javax.management.loading,javax.management.modelmbean,javax.management.monitor,javax.management.openmbean,javax.management.relation,javax.management.remote,javax.management.remote.rmi,javax.management.timer,javax.naming,javax.naming.directory,javax.naming.event,javax.naming.ldap,javax.naming.spi,javax.net,javax.net.ssl,javax.print,javax.print.attribute,javax.print.attribute.standard,javax.print.event,javax.rmi,javax.rmi.CORBA,javax.rmi.ssl,javax.script,javax.security.auth,javax.security.auth.callback,javax.security.auth.kerberos,javax.security.auth.login,javax.security.auth.spi,javax.security.auth.x500,javax.security.cert,javax.security.sasl,javax.sound.midi,javax.sound.midi.spi,javax.sound.sampled,javax.sound.sampled.spi,javax.sql,javax.sql.rowset,javax.sql.rowset.serial,javax.sql.rowset.spi,javax.swing,javax.swing.border,javax.swing.colorchooser,javax.swing.event,javax.swing.filechooser,javax.swing.plaf,javax.swing.plaf.basic,javax.swing.plaf.metal,javax.swing.plaf.multi,javax.swing.plaf.nimbus,javax.swing.plaf.synth,javax.swing.table,javax.swing.text,javax.swing.text.html,javax.swing.text.html.parser,javax.swing.text.rtf,javax.swing.tree,javax.swing.undo,javax.tools,javax.transaction,javax.transaction.xa,javax.xml,javax.xml.bind,javax.xml.bind.annotation,javax.xml.bind.annotation.adapters,javax.xml.bind.attachment,javax.xml.bind.helpers,javax.xml.bind.util,javax.xml.crypto,javax.xml.crypto.dom,javax.xml.crypto.dsig,javax.xml.crypto.dsig.dom,javax.xml.crypto.dsig.keyinfo,javax.xml.crypto.dsig.spec,javax.xml.datatype,javax.xml.namespace,javax.xml.parsers,javax.xml.soap,javax.xml.stream,javax.xml.stream.events,javax.xml.stream.util,javax.xml.transform,javax.xml.transform.dom,javax.xml.transform.sax,javax.xml.transform.stax,javax.xml.transform.stream,javax.xml.validation,javax.xml.ws,javax.xml.ws.handler,javax.xml.ws.handler.soap,javax.xml.ws.http,javax.xml.ws.soap,javax.xml.ws.spi,javax.xml.ws.spi.http,javax.xml.ws.wsaddressing,javax.xml.xpath,org.ietf.jgss,org.omg.CORBA,org.omg.CORBA_2_3,org.omg.CORBA_2_3.portable,org.omg.CORBA.DynAnyPackage,org.omg.CORBA.ORBPackage,org.omg.CORBA.portable,org.omg.CORBA.TypeCodePackage,org.omg.CosNaming,org.omg.CosNaming.NamingContextExtPackage,org.omg.CosNaming.NamingContextPackage,org.omg.Dynamic,org.omg.DynamicAny,org.omg.DynamicAny.DynAnyFactoryPackage,org.omg.DynamicAny.DynAnyPackage,org.omg.IOP,org.omg.IOP.CodecFactoryPackage,org.omg.IOP.CodecPackage,org.omg.Messaging,org.omg.PortableInterceptor,org.omg.PortableInterceptor.ORBInitInfoPackage,org.omg.PortableServer,org.omg.PortableServer.CurrentPackage,org.omg.PortableServer.POAManagerPackage,org.omg.PortableServer.POAPackage,org.omg.PortableServer.portable,org.omg.PortableServer.ServantLocatorPackage,org.omg.SendingContext,org.omg.stub.java.rmi,org.w3c.dom,org.w3c.dom.bootstrap,org.w3c.dom.css,org.w3c.dom.events,org.w3c.dom.html,org.w3c.dom.ls,org.w3c.dom.ranges,org.w3c.dom.stylesheets,org.w3c.dom.traversal,org.w3c.dom.views,org.w3c.dom.xpath,org.xml.sax,org.xml.sax.ext,org.xml.sax.helpers
eclipse.launcher: C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\eclipse.exe
eclipse.launcher.name: Eclipse
osgi.frameworkClassPath: ., file:C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.osgi.compatibility.state_1.0.200.v20160504-1419.jar
org.eclipse.swt.internal.deviceZoom: 100
equinox.use.ds: true
org.osgi.framework.language: fr
eclipse.pde.launch: true
user.language: fr
org.osgi.framework.processor: x86-64
osgi.syspath: c:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\plugins
osgi.compatibility.bootdelegation.default: true
sun.boot.library.path: C:\06-Java\jdk1.8.0_171\jre\bin
org.osgi.framework.system.capabilities: osgi.ee; osgi.ee="OSGi/Minimum"; version:List<Version>="1.0, 1.1, 1.2",osgi.ee; osgi.ee="JRE"; version:List<Version>="1.0, 1.1",osgi.ee; osgi.ee="JavaSE"; version:List<Version>="1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8",osgi.ee; osgi.ee="JavaSE/compact1"; version:List<Version>="1.8",osgi.ee; osgi.ee="JavaSE/compact2"; version:List<Version>="1.8",osgi.ee; osgi.ee="JavaSE/compact3"; version:List<Version>="1.8"
osgi.compatibility.bootdelegation: true
java.version: 1.8.0_171
org.osgi.framework.os.name: Windows10
user.timezone: America/New_York
sun.arch.data.model: 64
osgi.bundles: reference:file:C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.osgi.compatibility.state_1.0.200.v20160504-1419.jar,reference:file:C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.1.200.v20160504-1450.jar@1:start
java.endorsed.dirs: C:\06-Java\jdk1.8.0_171\jre\lib\endorsed
osgi.tracefile: C:\00-PROJECT-2020-UQAM-DEV-V2\runtime-EclipseApplication\.metadata\trace.log
sun.cpu.isalist: amd64
sun.jnu.encoding: UTF-8
eclipse.application: org.eclipse.ui.ide.workbench
file.encoding.pkg: sun.io
org.osgi.framework.vendor: Eclipse
equinox.init.uuid: true
file.separator: \
java.specification.name: Java Platform API Specification
osgi.checkConfiguration: true
java.class.version: 52.0
user.country: CA
org.eclipse.equinox.launcher.splash.location: C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\plugins\org.eclipse.platform_4.6.3.v20170301-0400\splash.bmp
osgi.configuration.cascaded: false
java.home: C:\06-Java\jdk1.8.0_171\jre
osgi.os: win32
eclipse.commands: -launcher
C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\eclipse.exe
-name
Eclipse
-showsplash
600
-product
org.eclipse.platform.ide
-data
C:\00-PROJECT-2020-UQAM-DEV-V2\uqam-dev-ws/../runtime-EclipseApplication
-configuration
file:C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/
-dev
file:C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/dev.properties
-os
win32
-ws
win32
-arch
x86_64
-nl
fr_CA
-consoleLog

java.vm.info: mixed mode
osgi.splashLocation: C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\plugins\org.eclipse.platform_4.6.3.v20170301-0400\splash.bmp
os.version: 10.0
osgi.arch: x86_64
path.separator: ;
java.vm.version: 25.171-b11
org.osgi.supports.framework.fragment: true
user.variant: 
osgi.framework.shape: jar
java.awt.printerjob: sun.awt.windows.WPrinterJob
osgi.instance.area.default: file:/C:/Users/Michel Heon/workspace/
sun.io.unicode.encoding: UnicodeLittle
org.osgi.framework.version: 1.8.0
awt.toolkit: sun.awt.windows.WToolkit
user.script: 
eclipse.stateSaveDelayInterval: 30000
osgi.dev: file:C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/dev.properties
osgi.install.area: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/
osgi.framework: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.osgi_3.11.3.v20170209-1843.jar
user.home: C:\Users\Michel Heon
org.eclipse.equinox.simpleconfigurator.configUrl: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/org.eclipse.equinox.simpleconfigurator/bundles.info
osgi.splashPath: file:C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/plugins/org.eclipse.platform_4.6.3.v20170301-0400
osgi.framework.useSystemProperties: true
eclipse.consoleLog: true
osgi.nl: fr_CA
java.specification.vendor: Oracle Corporation
java.vendor.url: http://java.oracle.com/
java.library.path: C:\06-Java\jdk1.8.0_171\bin;C:\WINDOWS\Sun\Java\bin;C:\WINDOWS\system32;C:\WINDOWS;C:/06-Java/jdk1.8.0_171/bin/../jre/bin/server;C:/06-Java/jdk1.8.0_171/bin/../jre/bin;C:/06-Java/jdk1.8.0_171/bin/../jre/lib/amd64;C:\Program Files\VanDyke Software\Clients\;C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\System32\WindowsPowerShell\v1.0\;C:\WINDOWS\System32\OpenSSH\;C:\Program Files\Git\cmd;C:\Program Files\Git\mingw64\bin;C:\Program Files\Git\usr\bin;C:\06-Java\apache-maven-3.6.1\bin;C:\Program Files\Git LFS;C:\Program Files\MySQL\MySQL Shell 8.0\bin\;C:\Users\Michel Heon\AppData\Local\Microsoft\WindowsApps;C:\Users\Michel Heon\AppData\Local\GitHubDesktop\bin;C:\Users\Michel Heon\AppData\Local\Microsoft\WindowsApps;;C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse;;.
eclipse.startTime: 1601036220199
eclipse.p2.data.area: @config.dir/.p2
org.osgi.framework.os.version: 10.0.0
java.vm.vendor: Oracle Corporation
java.runtime.name: Java(TM) SE Runtime Environment
java.class.path: C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\plugins\org.eclipse.equinox.launcher_1.3.201.v20161025-1711.jar
sun.java.command: org.eclipse.equinox.launcher.Main -launcher C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse\eclipse.exe -name Eclipse -showsplash 600 -product org.eclipse.platform.ide -data C:\00-PROJECT-2020-UQAM-DEV-V2\uqam-dev-ws/../runtime-EclipseApplication -configuration file:C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/ -dev file:C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/dev.properties -os win32 -ws win32 -arch x86_64 -nl fr_CA -consoleLog
org.eclipse.update.reconcile: false
java.vm.specification.name: Java Virtual Machine Specification
java.vm.specification.version: 1.8
sun.os.patch.level: 
sun.cpu.endian: little
gosh.args: --nointeractive
java.io.tmpdir: C:\Users\MICHEL~1\AppData\Local\Temp\
java.vendor.url.bug: http://bugreport.sun.com/bugreport/
applicationXMI: org.eclipse.ui.workbench/LegacyIDE.e4xmi
eclipse.product: org.eclipse.platform.ide
eclipse.home.location: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/eclipse-committers-neon-3/eclipse/
java.awt.graphicsenv: sun.awt.Win32GraphicsEnvironment
os.arch: amd64
java.ext.dirs: C:\06-Java\jdk1.8.0_171\jre\lib\ext;C:\WINDOWS\Sun\Java\lib\ext
user.dir: C:\00-PROJECT-2020-UQAM-DEV-V2\eclipse-committers-neon-3\eclipse
org.osgi.supports.framework.requirebundle: true
line.separator: 

java.vm.name: Java HotSpot(TM) 64-Bit Server VM
org.apache.commons.logging.Log: org.apache.commons.logging.impl.NoOpLog
file.encoding: UTF-8
eclipse.buildId: 4.6.3.M20170301-0400
java.specification.version: 1.8
org.osgi.framework.executionenvironment: OSGi/Minimum-1.0,OSGi/Minimum-1.1,OSGi/Minimum-1.2,JavaSE/compact1-1.8,JavaSE/compact2-1.8,JavaSE/compact3-1.8,JRE-1.1,J2SE-1.2,J2SE-1.3,J2SE-1.4,J2SE-1.5,JavaSE-1.6,JavaSE-1.7,JavaSE-1.8
osgi.configuration.area: file:/C:/00-PROJECT-2020-UQAM-DEV-V2/uqam-dev-ws/.metadata/.plugins/org.eclipse.pde.core/Eclipse Application/
osgi.logfile: C:\00-PROJECT-2020-UQAM-DEV-V2\runtime-EclipseApplication\.metadata\.log
