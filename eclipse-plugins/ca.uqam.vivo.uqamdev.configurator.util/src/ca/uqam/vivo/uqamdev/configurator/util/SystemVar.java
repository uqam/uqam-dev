package ca.uqam.vivo.uqamdev.configurator.util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.e4.core.services.log.Logger;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.osgi.framework.Bundle;

@SuppressWarnings("restriction")
public class SystemVar {
    private static Logger logger = PlatformUI.getWorkbench().getService(org.eclipse.e4.core.services.log.Logger.class);
    private static IStringVariableManager manager;

    public static void init() throws IOException {
        System.setProperty("sun.jnu.encoding", "UTF-8");
        System.setProperty("file.encoding", "UTF-8");
        manager = VariablesPlugin.getDefault().getStringVariableManager();
		setEclipseVars();
    }
	private static void setEclipseVars() {
		manager = VariablesPlugin.getDefault().getStringVariableManager();
		if (SystemVar.getOS().contains("win32")) {
			
			String key = "catalina_app";
			String value = "catalina.bat";
			setValueVariable(key, value);

			key = "solr_app";
			value = "solr.cmd";
			setValueVariable(key, value);

			key = "rm_cmd";
			value = "C:\\Program Files\\Git\\usr\\bin\\rm.exe";
			setValueVariable(key, value);
		} else {
			
			String key = "catalina_app";
			String value = "catalina.sh";
			setValueVariable(key, value);

			key = "solr_app";
			value = "solr.sh";
			setValueVariable(key, value);

			key = "rm_cmd";
			value = "/bin/rm";
			setValueVariable(key, value);
		}
		String key = "java_home";
		String value = SystemVar.getJavaHome();
		setValueVariable(key, value);
	}

	private static void setValueVariable(String key, String value) {
		IValueVariable[] vars = new IValueVariable[1];
		vars[0] = manager.getValueVariable(key);
		if (vars[0]!= null)   manager.removeVariables(vars);
		IValueVariable variable = manager.newValueVariable(key, "", false, value);
		try {
			manager.addVariables(new IValueVariable[] { variable });
		} catch (CoreException e1) {
		}

	}


    public static void dump() {
        Properties p = System.getProperties();
        Enumeration keys = p.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String value = (String) p.get(key);
            System.err.println(key + ": " + value);
        }
    }

    public static String getOS() {
        return System.getProperty("osgi.os");
    }
    public static String getJavaHome() {
        return System.getProperty("java.home");
    }

    public static String getEclipseHomeVar() throws CoreException {
        return manager.getDynamicVariable("eclipse_home").getValue("");
      
    }
}
