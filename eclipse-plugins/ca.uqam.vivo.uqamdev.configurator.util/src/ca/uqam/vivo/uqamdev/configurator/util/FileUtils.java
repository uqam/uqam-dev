package ca.uqam.vivo.uqamdev.configurator.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URI;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;

public class FileUtils {
	public static void copyFiles (File srcFolder, IContainer destFolder) throws CoreException, FileNotFoundException {
	    for (File f: srcFolder.listFiles()) {
	        if (f.isDirectory()) {
	            IFolder newFolder = destFolder.getFolder(new Path(f.getName()));
	            newFolder.create(true, true, null);
	            copyFiles(f, newFolder);
	        } else {
	            IFile newFile = destFolder.getFile(new Path(f.getName()));
	            newFile.create(new FileInputStream(f), true, null);
	        }
	    }
	}
}
