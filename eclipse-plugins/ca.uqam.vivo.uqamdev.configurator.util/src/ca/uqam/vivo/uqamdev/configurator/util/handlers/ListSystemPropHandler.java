package ca.uqam.vivo.uqamdev.configurator.util.handlers;

import java.util.Enumeration;
import org.apache.commons.lang.SystemUtils;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ListSystemPropHandler extends AbstractHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IPreferenceStore store = EditorsUI.getPreferenceStore();
		System.out.println("--");
		String lineNumbers = AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS; 
//		store.setValue(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS, true); 
		System.out.println("avant"+ store.getBoolean(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS));
		store.setValue(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS, true);
		System.out.println("après"+ store.getBoolean(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS));
		System.out.println("--");
//		Properties systemProperties = System.getProperties();
//		SortedMap sortedSystemProperties = new TreeMap(systemProperties);
//		Set keySet = sortedSystemProperties.keySet();
//		Iterator iterator = keySet.iterator();
//		while (iterator.hasNext()) {
//			String propertyName = (String) iterator.next();
//			String propertyValue = systemProperties.getProperty(propertyName);
//			System.out.println(propertyName + ": " + propertyValue);
//		}		
		return null;
	}
}
