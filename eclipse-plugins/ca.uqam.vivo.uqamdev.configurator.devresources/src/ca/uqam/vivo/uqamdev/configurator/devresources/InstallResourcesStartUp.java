package ca.uqam.vivo.uqamdev.configurator.devresources;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.osgi.framework.Bundle;

import ca.uqam.vivo.uqamdev.configurator.util.FileUtils;
import ca.uqam.vivo.uqamdev.configurator.util.SystemVar;

public class InstallResourcesStartUp implements IStartup {

	private IProject project;
	private NullProgressMonitor monitor;
	private Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
	private IWorkspace workspace;


	@Override
	public void earlyStartup() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				Job job = new Job("Configuring UQAM-DEV") {
					protected IStatus run(IProgressMonitor monitor) {
						try {
							SystemVar.init();
							monitor = new NullProgressMonitor();
							workspace= ResourcesPlugin.getWorkspace();    
							String projectName = "03-UQAM-DEV-Resources";
							project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
							IProjectDescription projetDescription = workspace.newProjectDescription(project.getName());
							if (!project.exists()) 
							{
								project.create(projetDescription, monitor);
								project.open(monitor);
							} else {
								return Status.OK_STATUS;
							}
							installResources();
							//							setupLaunchProduct(projectName);
						} catch (IOException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						} catch (CoreException e) {
							e.printStackTrace();
						}
						return Status.OK_STATUS;
					}
				};
				job.schedule();		
			}});
	}

//	private void setupLaunchProduct(String projectName) throws CoreException {
//		IProject prj = workspace.getRoot().getProject(projectName);
//		prj.refreshLocal(IResource.DEPTH_INFINITE , monitor);
//		IResource[] resources = prj.members(true);
//		for (int i = 0; i < resources.length; i++) {
//			IResource resource = resources[i];
//			if (resource instanceof IFile){
//				if(resource.getFileExtension().equals("launch")){
//					launchProduct((IFile) resource);
//				}
//			}
//		}
//	}

//	@SuppressWarnings("restriction")
//	private void launchProduct(IFile productFile) {
//		String mode = "run, debug";
//		IProductModel workspaceProductModel = new WorkspaceProductModel(productFile, false);
//		try {
//			workspaceProductModel.load();
//		} catch (CoreException e) {
//			PDEPlugin.log(e);
//		}
//		IProduct product = workspaceProductModel.getProduct();
//		new LaunchAction(product, productFile.getFullPath().toOSString(), mode).run();
//
//	}
//Launch snippet
//ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
//ILaunchConfigurationType launchType = manager.getLaunchConfigurationType("type id (from plugin.xml)");
//ILaunchConfigurationWorkingCopy wc = launchType.newInstance(null, manager.generateLaunchConfigurationName("Name Here"));
////						wc.setAttributes(launchAttributes);
//ILaunchConfiguration lc = wc.doSave();
//Launch launch = lc.launch(ILaunchManager.DEBUG_MODE, new NullProgressMonitor());

	private void installResources() throws URISyntaxException, IOException, CoreException {
		URI resUri = FileLocator.toFileURL(FileLocator.find(bundle, new Path("/resources"), null)).toURI();
		File resourcesFolder = new File(resUri);
		FileUtils.copyFiles(resourcesFolder, project);
	}

}
