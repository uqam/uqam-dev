package ca.uqam.vivo.uqamdev.configurator.tbcfe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.osgi.framework.Bundle;

import ca.uqam.vivo.uqamdev.configurator.util.SystemVar;
import ca.uqam.vivo.uqamdev.configurator.util.UnzipUtility;
import ca.uqam.vivo.uqamdev.configurator.util.WorkbenchUtils;

public class Startup implements IStartup {
	private static ProgressBar  progressBar;
	private boolean reboot = false;


	@Override
	public void earlyStartup() {
		WorkbenchPlugin.getDefault().getPreferenceStore().setValue("RUN_IN_BACKGROUND", false);
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {

				Job job = new Job("Installing Topbraid Composer") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						try {
							SystemVar.init();
//							System.err.println("Begin installTBCFE");
							monitor.beginTask("Install TBCME", 100);
							installTBCFE(monitor);
//							System.err.println("End installTBCFE");
						} catch (URISyntaxException | IOException | CoreException | InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						return Status.OK_STATUS;
					}

				};
				job.schedule();
				try {
					job.join();
					if (reboot) 								
						WorkbenchUtils.readyToRestart();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}});

	}
	private void installTBCFE(IProgressMonitor monitor) throws URISyntaxException, IOException, CoreException, InterruptedException {
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		UnzipUtility unzipper = new UnzipUtility();
		String resourceFn = FileLocator.toFileURL(FileLocator.find(bundle, new Path("/resources/"), null)).toURI().getRawPath().toString();
		String[] serverList = new File(resourceFn).list();
		String destDirectory = SystemVar.getEclipseHomeVar()+"dropins";
		for (String serverFileName : serverList) {
			String resFileName = destDirectory+"/"+serverFileName;
			File serverName = new File(resFileName.substring(0, resFileName.lastIndexOf('.')));
			if (!serverName.exists() && serverFileName.endsWith(".zip")) {
				unzipper.unzip(resourceFn+serverFileName, destDirectory, monitor);
				reboot =true;
				TimeUnit.SECONDS.sleep(5);
			}
		}
	}
}
