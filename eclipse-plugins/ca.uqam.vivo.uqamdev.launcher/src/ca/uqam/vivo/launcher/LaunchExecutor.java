package ca.uqam.vivo.launcher;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.pde.internal.core.iproduct.IProduct;
import org.eclipse.pde.internal.core.iproduct.IProductModel;
import org.eclipse.pde.internal.core.product.WorkspaceProductModel;
import org.eclipse.pde.internal.ui.PDEPlugin;
import org.eclipse.pde.internal.ui.launcher.LaunchAction;

@SuppressWarnings("restriction")
public class LaunchExecutor {

    private void launchProduct(IFile productFile) {
        String mode = "run, debug";
        IProductModel workspaceProductModel = new WorkspaceProductModel(productFile, false);
        try {
            workspaceProductModel.load();
        } catch (CoreException e) {
            PDEPlugin.log(e);
        }
        IProduct product = workspaceProductModel.getProduct();
        new LaunchAction(product, productFile.getFullPath().toOSString(), mode).run();
    }

}