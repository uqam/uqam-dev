package ca.uqam.vivo.launcher;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.debug.core.ILaunch;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.model.LaunchConfigurationDelegate;

public class ExampleLaunchConfigurationDelegate extends LaunchConfigurationDelegate{
    @Override
    public void launch(ILaunchConfiguration configuration, String mode, ILaunch launch, IProgressMonitor monitor)
            throws CoreException {
        String attribute = configuration.getAttribute(SampleLaunchConfigurationAttributes.CONSOLE_TEXT, "Simon says \"RUN!\"");
        System.out.println(attribute);
    }
}
