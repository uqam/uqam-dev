package ca.uqam.vivo.uqamdev.launcher;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Enumeration;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.pde.internal.core.iproduct.IProduct;
import org.eclipse.pde.internal.core.iproduct.IProductModel;
import org.eclipse.pde.internal.core.product.WorkspaceProductModel;
import org.eclipse.pde.internal.ui.PDEPlugin;
import org.eclipse.pde.internal.ui.launcher.LaunchAction;
import org.eclipse.ui.IStartup;
import org.osgi.framework.Bundle;

public class Startup implements IStartup {


	@SuppressWarnings("deprecation")
	@Override
	public void earlyStartup() {
		String pluginId = "ca.uqam.vivo.uqamdev.launcher";
		String mode = "run, debug";
		Bundle bundle = Platform.getBundle(pluginId);
		String filePath = "01-Start VIVO (tomcat).launch";
		Enumeration<URL> entries = bundle.findEntries("", "*.launch", true);
		while (entries.hasMoreElements()) {
			try {
				URL entry = (URL) entries.nextElement();
				URL entryUrl = FileLocator.toFileURL(entry);
				IWorkspace workspace= ResourcesPlugin.getWorkspace();
				IProject project = workspace.getRoot().getProject("External Files");
				if (!project.exists())
					project.create(null);
				if (!project.isOpen())
					project.open(null);
//				IPath location = new Path(entryUrl.getFile());
//				IFile iFile = project.getFile(location.lastSegment());
//				iFile.createLink(location, IResource.NONE, null);
//				launchProduct(iFile);
				new LaunchAction(null, entryUrl.getFile(), "run, debug");
			} catch (CoreException | IOException e) {
				// TODO Auto-generated catch block
//				e.printStackTrace();
			}
		}
	}
	private void launchProduct(IFile productFile) {
		String mode = "run, debug";
		IProductModel workspaceProductModel = new WorkspaceProductModel(productFile, false);
		try {
			workspaceProductModel.load();
		} catch (CoreException e) {
			PDEPlugin.log(e);
		}
		IProduct product = workspaceProductModel.getProduct();
		new LaunchAction(product, productFile.getFullPath().toOSString(), mode).run();
	}
}
