package ca.uqam.vivo.uqamdev.configurator.servers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.pde.internal.core.iproduct.IProduct;
import org.eclipse.pde.internal.core.iproduct.IProductModel;
import org.eclipse.pde.internal.core.product.WorkspaceProductModel;
import org.eclipse.pde.internal.ui.PDEPlugin;
import org.eclipse.pde.internal.ui.launcher.LaunchAction;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.WorkbenchException;
import org.eclipse.ui.editors.text.EditorsUI;
import org.eclipse.ui.preferences.ScopedPreferenceStore;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.texteditor.AbstractDecoratedTextEditorPreferenceConstants;
import org.osgi.framework.Bundle;


import ca.uqam.vivo.uqamdev.configurator.util.SystemVar;
import ca.uqam.vivo.uqamdev.configurator.util.*;

@SuppressWarnings("restriction")
public class Startup implements IStartup {
	Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
	private UnzipUtility unzipper;
	private File eclipseDir; // see getEclipseDir();
	private IStringVariableManager manager;
	private IProject project;
	private IWorkspace workspace;
	private String workSpaceLocation;
	private String launchDir;
	private NullProgressMonitor monitor;
	private boolean firstTime=false;
	@Override
	public void earlyStartup() {
		Display.getDefault().asyncExec(new Runnable() {
			public void run() {
				Job job = new Job("Configuring UQAM-DEV servers") {
					@Override
					protected IStatus run(IProgressMonitor monitor) {
						//							System.err.println("Begin Servers");
						unzipper = new UnzipUtility();
						try {
							SystemVar.init();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						try {
							ScopedPreferenceStore store = (ScopedPreferenceStore) EditorsUI.getPreferenceStore();
							store.setValue(AbstractDecoratedTextEditorPreferenceConstants.EDITOR_SPACES_FOR_TABS, true);
							store.save();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						workspace= ResourcesPlugin.getWorkspace();    
						workSpaceLocation = workspace.getRoot().getLocation().toOSString();						
						try {
							installWebServers();
							createProjects();
							setupSplash();
							if (firstTime) {
								WorkbenchUtils.readyToRestart();
							}
							IWorkbench workbench = PlatformUI.getWorkbench();
							IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
							//							workbench.getPerspectiveRegistry().setDefaultPerspective("org.eclipse.ui.resourcePerspective");					
						} catch (CoreException e) {
							e.printStackTrace();
						} catch (URISyntaxException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
						//							System.err.println("End Servers");
						return Status.OK_STATUS;
					}


					private void setupSplash()  {
						Bundle targetBundle = Platform.getBundle("org.eclipse.platform");
						String targetFn = null;
						String sourcesFn = null;

						try {
							Bundle srcBundle = Platform.getBundle("ca.uqam.vivo.uqamdev.configurator.devresources");
							sourcesFn = FileLocator.resolve(srcBundle.getEntry("/splash.bmp")).toURI().getPath();
							targetFn = FileLocator.resolve(Platform.getBundle("org.eclipse.platform").getEntry("/")).toURI().getPath().concat("splash.bmp");
							org.apache.commons.io.FileUtils.copyFile(new File(sourcesFn), new File(targetFn));
						} catch (URISyntaxException | IOException | NullPointerException e) {
							Activator.getDefault().getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Source filename = "+sourcesFn));
							Activator.getDefault().getLog().log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Target filename = "+targetFn));
							System.err.println("Source filename = "+sourcesFn);
							System.err.println("Target filename = "+targetFn);
							e.printStackTrace();
						}
					}


					private void createProjects() throws CoreException {
						monitor = new NullProgressMonitor();
						createAProject("00-VIVO_HOME", "vivo/home");
						createAProject("01-TOMCAT_WEBAPPS", "apache-tomcat-8.5.11/webapps");
						createAProject("02-SORL_SERVER", "solr-7.7.2/server/solr");
					}

					private void installWebServers() throws URISyntaxException, IOException, CoreException {
						String resourceFn = FileLocator.toFileURL(FileLocator.find(bundle, new Path("/resources/servers/"), null)).toURI().getRawPath().toString();
						String[] serverList = new File(resourceFn).list();
						String destDirectory = SystemVar.getEclipseHomeVar();
						//            String destDirectory = Path.fromPortableString(eclipseDir.getAbsolutePath().toString()).toOSString();
						for (String serverFileName : serverList) {
							String resFileName = resourceFn+serverFileName;
							String serverName = serverFileName.substring(0, serverFileName.lastIndexOf('.'));
							if (!(new File(destDirectory+serverName).exists())&& resFileName.endsWith(".zip")){
								firstTime = true;
								unzipper.unzip(resFileName, destDirectory);
							} else {
								return;
							}
						}

					}

				};
				job.schedule();
			}});
	}
	private void createAProject(String projectName, String linkPath) throws CoreException {
		project = ResourcesPlugin.getWorkspace().getRoot().getProject(projectName);
		if (!project.exists()) 
		{
			IProjectDescription projetDescription = workspace.newProjectDescription(project.getName());
			projetDescription.setLocationURI(new File(SystemVar.getEclipseHomeVar()+linkPath).toURI());
			project.create(projetDescription, monitor);
			project.open(monitor);
		}

	}

	private void getEclipseDir() throws IOException, CoreException {
		IPath eclipseHome = new Path(SystemVar.getEclipseHomeVar());
		eclipseDir = eclipseHome.toFile().getParentFile();
		launchDir = workSpaceLocation+"/.metadata/.plugins/org.eclipse.debug.core/.launches";
		java.nio.file.Path launchPath = Paths.get(launchDir);
		if (!Files.exists(launchPath)) {
			Files.createDirectory(launchPath);
		} 
	}

	private void launchProduct(IFile productFile) {
		String mode = "run, debug";
		IProductModel workspaceProductModel = new WorkspaceProductModel(productFile, false);
		try {
			workspaceProductModel.load();
		} catch (CoreException e) {
			PDEPlugin.log(e);
		}
		IProduct product = workspaceProductModel.getProduct();
		new LaunchAction(product, productFile.getFullPath().toOSString(), mode).run();
	}

	//			private void installWebServers() throws IOException, URISyntaxException, CoreException {

	//
	//		
	//		/*
	//		 * Phase 1 Vérifier si tomcat et solr sont installés
	//		 */
	//		File[] matches = eclipseDir.listFiles(new FilenameFilter()
	//		{
	//			public boolean accept(File dir, String name)
	//			{
	//				return name.contains("apache");
	//			}
	//		});
	//		if (matches.length == 0) {
	//			/*
	//			 * Phase 2 Décompresser
	//			 */
	//			String resourceFn = FileLocator.toFileURL(FileLocator.find(bundle, new Path("/resources/servers/"), null)).toURI().getRawPath().toString();
	//			String[] serverList = new File(resourceFn).list();
	//			String destDirectory = SystemVar.getEclipseHomeVar();
	//			//            String destDirectory = Path.fromPortableString(eclipseDir.getAbsolutePath().toString()).toOSString();
	//			for (String pathname : serverList) {
	//				if (!(new File(resourceFn+pathname).exists()))
	//				unzipper.unzip(resourceFn+pathname, destDirectory);
	//			}
	//		}
	//			}
}	
