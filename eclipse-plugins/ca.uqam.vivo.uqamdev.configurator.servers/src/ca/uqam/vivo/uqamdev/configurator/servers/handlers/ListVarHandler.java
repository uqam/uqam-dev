package ca.uqam.vivo.uqamdev.configurator.servers.handlers;

import java.util.Enumeration;
import java.util.Properties;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.variables.IDynamicVariable;
import org.eclipse.core.variables.IStringVariable;
import org.eclipse.core.variables.IStringVariableManager;
import org.eclipse.core.variables.IValueVariable;
import org.eclipse.core.variables.VariablesPlugin;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.eclipse.ui.handlers.HandlerUtil;

import ca.uqam.vivo.uqamdev.configurator.util.SystemVar;

public class ListVarHandler extends AbstractHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        //		MessageDialog.openInformation(
        //				window.getShell(),
        //				"Configurator",
        //				"Hello, Eclipse world");
        MessageConsole myConsole = findConsole("UQAM-DEV");
        MessageConsoleStream out = myConsole.newMessageStream();
        Properties p = System.getProperties();
        Enumeration keys = p.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            String value = (String) p.get(key);
            out.println(key + ": " + value);
        }
        IStringVariableManager manager = VariablesPlugin.getDefault().getStringVariableManager();
        IValueVariable[] values = manager.getValueVariables();
        IStringVariable[] vars = manager.getVariables();
        out.println("//////////////////////");
        out.println("//  VARIABLES       //");
        out.println("//////////////////////");
        out.println(VariablesPlugin.getDefault().getStateLocation().toOSString());
        for (int i = 0; i < vars.length; i++) {
            try {
                String name = vars[i].getName();
                out.print(name);
                if (name.contains("eclipse")
                        || name.contains("resource")
                        || name.contains("workspace")
                        || name.contains("loc")
                        || name.contains("java")
                        || name.contains("path")
                        || name.contains("home")
                        || name.contains("catalina")
                        )  out.print( " " + manager.getDynamicVariable(name).getValue(""));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                out.print(" ****Variable references non-existent resource****");
//                e.printStackTrace();
            }
            out.println("");
        }

        return null;
    }
    private MessageConsole findConsole(String name) {
        ConsolePlugin plugin = ConsolePlugin.getDefault();
        IConsoleManager conMan = plugin.getConsoleManager();
        IConsole[] existing = conMan.getConsoles();
        for (int i = 0; i < existing.length; i++)
            if (name.equals(existing[i].getName()))
                return (MessageConsole) existing[i];
        //no console found, so create a new one
        MessageConsole myConsole = new MessageConsole(name, null);
        conMan.addConsoles(new IConsole[]{myConsole});
        return myConsole;
    }
}
